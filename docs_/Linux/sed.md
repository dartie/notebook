Title:      Sed
desc:       Sed
template:   document
nav:        Linux>Sed
date:       2018/01/19

```
sed -i 's/original/new/g' file.txt
```

Explanation:
````sed```` = Stream EDitor
````-i```` = in-place (i.e. save back to the original file)

The command string:
````s```` = the substitute command
````original```` = a regular expression describing the word to replace (or just the word itself)
````new```` = the text to replace it with
````g```` = global (i.e. replace all and not just the first occurrence)
````file.txt```` = the file name

Additional arguments:
````-r```` = add regular expressions



## Reference: https://www.thegeekstuff.com/2009/09/unix-sed-tutorial-replace-text-inside-a-file-using-substitute-command/?utm_source=sitekickr&utm_medium=snip_button