Title:      English
desc:       English
template:   document
nav:        English>English
date:       2019/03/12

# Inglese


Difference between as well and either


## Words to add:


Slightly


bring up


odd invece di "weird"


At this stage


for instance


straight away


on going instead of outbound


Rather than instead of "instead of"


whose rather then "in which"


Massive


carry on


Break down rather than split










## Pronunciation:


filter


active


enhancement


parents


Abbandoned


Alternatively


peach/pitch


want/won't


mail/male


face/phase


bit/beat


peace/peas


weak/week


Sheep/ship


Word/ward


pool/poll


got/goat


thank/tank


For/fore


Plan/plain


Prendere esempio


Whether/weather


flow/flaw/floor


bought/both/boat/bot


hand/end/and


suck/sack


grid/greed


Weight/wait


regex : [reghex]










## Words:
from moving forward : d'ora in poi

naive = ingenuo, candido

to nods : annuire (con la test: to nod head)


auction [oction] : vendita all'asta


thrifty - frugal : parsimonioso


hamper - hinder: intralcio/intralciare


pun [pan] : gioco di parole


stoned : cotto (stanco)


asbestos : amianto


sneaky : vago


upfront : diretto / anticipato


taming : monotono, addomesticato, domestico


pussycat : innocuo


muratore : construction worker or bricklayer


eco : echo


Antonyms : opposto


"Speaking of which" means "while we are talking about this subject"


bargain : trattativa


burden : fardello


It is a bit of a challenge : whatever is being referred to is not easy to achieve.


Bear with me : have patience with me


backtick : `


tracheitis [trechi-aitis]


**goloso:** sweet tooth


**elevenses:** a short break for light refreshments, usually with tea or coffee, taken at about eleven o'clock in the morning.

**excruciating [ɪkˈskruːʃɪeɪtɪŋ] :** straziante


## Writing to fix:


luggage


baggage






1. I am gutted
2. Ta.
3. I am knackered
4. Let’s play it by ear
5. I’m minted
6. Dodgy






brimming with confidence = pieno di se stesso


how are you coping? = come stai affrontando?


raped = violentata


fag = sigaretta


bathe = farsi il bagno (did you bathe.. ?)


fuorviare⇒vtr(sviare) = mislead


snap your fingers = schioccare le dita






tailored = su misura (tailored shirt) [teiled]


silk = seta






Splinter: spina


Tweezers: pinzetta


Gauze : garza (goosz)






mi faro' perdonare=I'll make up for it.


Sorry but I punctured a tyre that's why I'm late. = forare la ruota


nail-damaged tire


pierce the wheel


punch






daft


harrowing


shattered


shag


pull over


smack


sinking


bent 


swan 


belch


grief = dolore


lapsed


chasm


stitch up


aberration


groom


out of blue = event that occurs unexpectedly


abducted = rapimento


tampered = manomessi


fart = scorreggia






Spot on : correct / accurate






Umido=moist, damp


Fag=ricchione


Loathing=odio


Boor=incivile


Cellar=cantina


wary =diffidente


Startle=far venire un colpo


Prank=biricchinata


Polished=tirata a lucido


Puny=fiacco, debole


Eavesdrop/overhear=origliare






## FOOD:


turnip tops: cime di rapa


aragosta: lobster


calamaro: squid






http://painintheenglish.com/case/4751






I'll be down again every now and then : Tornerò di nuovo ogni tanto










entail : implicare


inherit : ereditare


in turn : a sua volta


subire: endure


indeciso: uncertain, hesitant


imbranato: dolt, clod


sbuffare: snort


grumble: brontolare


: bisbigliare


Impalcatura: scaffolding


incentivize: incentivare


a sua volta : http://www.wordreference.com/iten/a%20loro%20volta






withdrawn/withdrawal


whinge: piagnisteo


whereby : per cui






assertion = affermazione


let down your guard = abbassare la guardia


spazzola - pettine = brush, comb


embellish: abbellire






dentiera: dentures npl dental plate, dental bridge


bite: mouthguard






spalancare⇒ vtr(aprire completamente)open wide vtr phrasal sep spread out vtr phrasal insep thrust open vtr phrasal sep






rimproverare⇒ vtr(sgridare)scold, reprimand vtr tell [sb] offrimproverare vtr(rinfacciare)reproach vtr


(colloquial)


throw back in [sb]'s face idiom blame vtrrimproverarsi vtr(sentirsi responsabile di)blame yourself vtr + refl reproach yourself vtr + refl






rinfacciare⇒ vtr(ricordare a qn cosa si è fatto per lui)bring up, hold against vtr


(colloquial)


rub in [sb]'s face expr throw back in [sb]'s face exprrinfacciare vtrfigurato (rimproverare aspramente)reproach, reprimand vtr


(colloquial)


rub in vtr










Using exec and eval in Python is highly frowned upon. --> is highly frowned upon --> e' disapprovato






cutlary (uk) - flatware (us): stoviglie






Leggere 200x80: 200by80 or??


doga di un letto: slat


tongue in cheek? : ironia?






http://www.bbc.co.uk/learningenglish/english/features/pronunciation


cough - /kɒf/


though - /ðəʊ/


rough - /rʌf/


through - /θruː






## ATTREZZI:


zip gun:


hammer: martello






sorrow: pena, dolore






ripido: steep


slippery: scivoloso


tratto (strada): stretch of road






presumptuous: presuntuoso


humble: modesto










twist = rotolare, attorcigliare


pigeons steps: piedi di piombo






Out of my hands: fuori dal mio controllo






ci sono stati = there have been


inquinato = polluted






barrare = strike through -----> barrato : struck through






capra-capretta = goat


loosen up = allentare


pounded = pestare/martellare


innaffiare = to water






viziato = tampered/spoiled






to be accountable : dar conto


difetto: flaw, lack






Hang the clothes out to dry = stendere i panni


Cognizance = consapevolezza (prendere atto = I take cognizance of this)






Smart aleck: sapientone


Humble: modesto






Sprinkle: spruzzare






quote: citazione, battuta


punctuation : [panctuetion] punteggiatura






spiced : speziato


spicy : piccante






## Adjectives:


crispy = croccante


crunchy = croccante


cruncy = al dente


crappy = scadente


Creepy = scadente, raccapricciante


cruncy


mince (mains) = tritato






sibling = fratello/sorella






sheath = fodero


rag = straccio






inavvertitamente : inadvertently, accidentally


disprezzare : despise






Lasciare la parola: give the floor to or leave the floor to






discover by chance : or "by case" or accidentally : scoprire per caso






catch off guard : cogliere alla sprovvista


I was caught completely off guard when the police arrived with a search warrant.






threaten = minacciare


treat






and so forth = e cosi via


come across : imbattersi


Never forgetting or never forget


Neanche un bicchiere d'acqua






hindrance : ostacolo


marker : valutazione --> the marking criteria: i criteri di valutazione






Task 2 is worth twice as much as Task 1: Task 2 vale il doppio di quanto Task 1.


Ho rischiato di non partire : I almost did not go






conversely: al contrario






dare : osare, permettere (come ti permetti..?)


outstanding : fuori dal comune, eccezionale






## TH or GH words:


though : anche	[pron.: dou]


thought : pensiero - pensato	[pron.: thoutt]


tough : pesante, difficile	[pron.: taf]


taught : insegnato [pron.: toot]


throw : buttare	[pron.: throu]


through : attraversò	[pron.: tru]	[attenzione, true si legge ctru --> true ......through]






altough: benche	[pron.: altoo]	- after a subject and a verb.


although: anche se, sebbene	[pron.: oldoo]


‘in spite of’: after ‘in spite of’ we use a noun or a pronoun


after ‘although’ we use a subject and a verb, whereas after ‘in spite of’ we just use a noun or pronoun.


E.g.:


[__although_] we are influenced by advertising, we are free to make our own choices when we buy things.


[__in spite of_] increased profits, some companies ask their staff to work longer hours.






## PROVERBI:


the early bird catches the worm : chi tardi arriva male alloggia






for instance: to give an example 


in particular: to talk about a specific example.


namely: we use ‘namely’ to give specific detailed information about a subject that we are discussing.






nuance : sfumatura


neat: pulito, ordinato


hook up : allacciare


trip up : inciampare


melt : sciogliere






I will walk (or drive) you through what needs to be changed : Io vi guiderò attraverso ciò che deve essere cambiato.


glossy and matte paper =






Pane: bread


croccante: crisp, crunchy


briciole: crumbs


loaf: pagnotta, sfilatino


Farina:


cruncy : al dente






rain check : rimandare l'invito






power strip : scarpetta elettrica


umpteenth : ennesimo


since: quindi


peroxide : acqua ossigenata [pronuncia: peroseid]


rimproverare : scold, reprimand


to pinpoint : individuare






outbound - return : andata - ritorno






Nastro trasportatore = baggage carousel


Appeso = pendent, suspended, hanging


Pronuncia icon


Non tornerò più


Stringere con forza = wring






## Sinonimi e contrari


Enforces : fa rispettare, impone, applica


ripe - unripe : maturo - acerbo






aubergine


brewer's yeast: lievito


baking powder: bicarbonato di sodio


sopportare: support, bear, sustain


questo caso non è previsto dalla legge: the law makes no provision for such a case






to provide sb[QUALCUNO] with sth[QUALCOSA]


provide sth[QUALCOSA] for sb[QUALCUNO]






tyres: pneumatici


slashed : sqarciati


saddle : sella






fare una battuta: to crack a joke, make a witty


battuta (copione) : line






ormai: by now (ormai è tardi: it's late by now)






1-2 (calcio) : Push-and-run, also known as a one-two, a wall pass or a give-and-go


acqua ossigenata : peroxide (pron. peroksed)


Scivolare : slip , slide


http://www.one-tab.com/page/NpRc3PycTA-4w3Kx4kQJNQ






“would you like to come to the pub for a drink?”


“thanks but I think I’ll take a raincheck.”


E’ un modo per dire di no ma lasciando sottintendere che è rimandato alla prossima volta.






Figura di merda: to look like a tit : look like a tit


brutta figura: poor figure


to follow up : dare un seguito


Fasten : allacciare


Bound


Bond


Legare


To whom it may concern : per chi e coinvolto


http://www.englishpronunciationpod.com/podcast_15.html


http://britishenglishcoach.com/33-ways-to-speak-better-english-without-taking-classes/


TO RUN OUT OF = finire (fuel, time etc)


to get rid of = sbarazzarsi


outcome = risultato


end - finish - complete : http://www.ilmioinglese.com/2009/01/la-differenza-tra-end-e-finish-in-inglese/


refrain = astenersi


Statua statue


encounter = incontrare


rise up = sollevare, alzare (intransitivo (il sole si alza -  the government is going to raise taxes) )


raise up = sollevare, alzare (transitivo: il governo alza le tasse, oppure, the value of my shares is rising)


Mi raccomando


Investire


Ci tengo molto a te


Essere in anticipo


Fino a quando?


Internship


Superficiale


Emozionato


Guadagno


Ricavo


Loan = mutuo






Pierre-Edouard is away on business trip for the rest of the


week, hence










I'll take over on this.






Draw


Choose


Imitare






Delivery room : sala parto


Look out : guardare vegliare






Expectation


Prelievo = prelievonf(ritiro di denaro)withdrawal n


Versamento = deposit o pay in






Screw


Pay in






Pick off = eliminare


Give up = arrendersi


climb = salire le scale






harm=ferire - far male


relieve = alleviare --> rilieved = sollevato






for god sakes = per l'amor di Dio






Ing


Diff tra each other e piu d 2 xsone


Lie


Lay


Rush






premettere⇒ vtr


(dire parole introduttive)


introduce, start by saying, start by making vtr










advance, broach vtr






Premetto alcuni chiarimenti al discorso.






I will start by making a few clarifications to the argument.






## PHRASAL VERBS:


shut out : escludere


run over : investire


beat up : pestare / a pezzi(adj)


show up : presentarsi


set out : mettersi in viaggio


free up : liberare


keep up : stare al passo


blew-blew-blown : soffiare


blow up [solo in questa forma] : saltare in aria


blow off [solo in questa forma] : spazzare via o scorreggiare


beat up : picchiare


break up : lasciarsi(coppia), disintegrarsi


passed out : svenire, terminare addestramento


burn up : scottare


slip away : svanire


Mix up : scambiare


write off : senza speranza oppure sfasciare


Swan off :






Roll on


Wait up






Smash up


Splinting up


Spin off: scorporare


buckle up : allacciare le cinture (buckle: serrare)


burn away : andare a fuoco






## ALTRO:


flattered = lusingato


worthless = senza valori


against = contro


unforeseen = imprevisto, inatteso agg


how far = quanto lontano? fino a che punto?


wander = vagare, passeggiare


worth = lodevole


thorough = scrupoloso


though = eppure, sebbene


taught = past teach


throw = buttare


through = attraverso






Bias: pregiudizio


Climate: clima


Lay - laid - laid : disporre, preparare


Lay out : sistemare


Straightforward : lineare, chiaro






alley: vicolo


pull yourself together: Fatti coraggio


Proof prova


Anger rabbia






Ex - former


Is entitled : ha il diritto


Entailes : comportare






## TO LEARN


pronunciare I (i)


ascolto pronuncia






<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEwMzYxNTQ5NzYsLTE2MTE5ODcyNjcsMT
I1ODUwNjMwLDIxMDIyMTE2MDJdfQ==
-->