var toggler = document.getElementsByClassName("caret");
var i;

for (i = 0; i < toggler.length; i++) {
  toggler[i].addEventListener("click", function() {
    this.parentElement.querySelector(".nested").classList.toggle("active");
    this.classList.toggle("caret-down");
  });
}


function expand_collapse_all(mode="expand"){
    for (i = 0; i < toggler.length; i++) {
        if( mode === "expand"){
            toggler[i].parentElement.querySelector(".nested").classList.add("active");
            toggler[i].classList.add("caret-down");
        }
        else{
            toggler[i].parentElement.querySelector(".nested").classList.remove("active");
            toggler[i].classList.remove("caret-down");
        }
    }
}
