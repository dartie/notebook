function set_emoji(){
/*
<p><emoji>arrow_right</emoji></p> -> <p><emoji><i class="em em-anguished"></i></emoji></p>
*/

  emoji_elements = document.getElementsByTagName('emoji');

  for (i=0; i < emoji_elements.length; i++){
    emoji = emoji_elements[i].innerText;
    i_tag = document.createElement("i");
    i_tag.classList.add("em");
    i_tag.classList.add("em-" + emoji);
    emoji_elements[i].innerText = '';
    emoji_elements[i].appendChild(i_tag);
  }

}
//set_emoji()

function add_emoji(){
    let regex_emoji = new RegExp(":([a-zA-Z0-9\+\-_]+):", "gi");
  //let doc_tag = document.body.getElementsByTagName("*");
  //let doc_tag = document.getElementsByClassName('doc')[0]; /* in case the document is embedded in the notebook */
    let doc_tag = document.getElementById("doc").getElementsByTagName('*');

    for (i=0; i < doc_tag.length; i++){
        /* debug */
        if (doc_tag[i].outerHTML.includes(":arrow_right:")){
            ; /* console.log('found'); */
        }
        /* /debug */

        if (doc_tag[i].tagName == 'CODE' || doc_tag[i].tagName == 'PRE' || doc_tag[i].classList.contains("highlight")){
            continue;
        }

        doc_tag[i].outerHTML = doc_tag[i].outerHTML.replace(regex_emoji, '<emoji><i class="em em-$1"></i></emoji>');
    }
}

add_emoji();

