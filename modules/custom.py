import socket
import ipgetter
from colorama import init, Fore, Back, Style
init(convert=False, strip=False)

color_reset = Style.RESET_ALL
color_label = Fore.RED + Style.BRIGHT
color_info = Fore.CYAN + Style.BRIGHT
color_table = Fore.GREEN + Style.BRIGHT


def getHostname():
    hostname = socket.gethostname()

    return hostname


def getIpAddresses(): 
    addrList = socket.getaddrinfo(socket.gethostname(), None) 

    ipv4List=[]
    ipv6List=[] 
    for item in addrList: 
        #print("Item:", item)
        ip = item[4][0]
        if ':' in ip :
            ipv6List.append(item[4][0])
        else:
            ipv4List.append(item[4][0])

    return ipv4List #, ipv6List


def getExtIPAddress(): 
    ExtIP = ipgetter.myip()
    
    return ExtIP


def print_Network_info():
    print('\n' * 2 + f'{color_table}-' * 100)
    print(f'{color_label}Local IP Address/es: {color_info}{getIpAddresses()}{color_reset}')
    print(f'{color_label}Hostname: {color_info}{getHostname()}{color_reset}')
    print(f'{color_label}External IP Address: {color_info}{getExtIPAddress()}{color_reset}')
    print(color_reset)