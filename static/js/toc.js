/*
This script assigns a id to each hN element, in order to work with TOC
*/

function set_toc_progressive() {
    /*
    assigns a progressive number to each header id below specified.
    This requires that TOC links are based on the progressive number from the back end side (toc_toc-N)
    */
    headers = document.querySelectorAll("h1, h2, h3")

    for (i=0; i < headers.length; i++){
        headers[i].id = 'toc-' + i;
    }

}


function set_id_chapter(){
    /*
    assigns the innerText to each header id below specified.
    This requires that TOC links are based on innerText from the back end side (toc_tocName)
    example:  chapter text: "Installation Notes" -> header id = "installation-notes"
    */    headers = document.querySelectorAll("h1, h2, h3")
    unaccepted_chars = ['%', '?', "{", "}", "|", "\\", "^", "[", "]", "`", ";", "/", "?", ":", "@", "&", "=", "+", "$", ",", " "];

    for (i=0; i < headers.length; i++){
        let link = headers[i].innerText;
        for (j=0; j < unaccepted_chars.length; j++){
            regex_replace = new RegExp('\\' + unaccepted_chars[j], "g");
            link = link.toLowerCase().replace(regex_replace, '-')
        }

        headers[i].id = link;
    }
}

//set_toc_progressive();
set_id_chapter()