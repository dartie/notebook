Title:      Flask
desc:       Flask
template:   document
nav:        Python>Flask>Flask
date:       2019/02/18

* `Flask Error: “Method Not Allowed The method is not allowed for the requested URL”`
In 
```python
@app.route('/entry', methods=['GET', 'POST'])
```
`methods` does not contain the method used