
li_elements = document.getElementsByTagName('li');

for(i=0; i < li_elements.length; i++){
    if (li_elements[i].innerHTML.startsWith('[ ]')){
         li_elements[i].innerHTML = li_elements[i].innerHTML.replace('[ ]', '<input type="checkbox">')
    }
    else if (li_elements[i].innerHTML.toLowerCase().startsWith('[x]')){
        li_elements[i].innerHTML = li_elements[i].innerHTML.replace('[x]', '<input type="checkbox" checked>').replace('[X]', '<input type="checkbox" checked>')
    }
}