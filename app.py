import sys
import requests
import re
sys.path.append("..")
sys.path.append("modules")
import os
import io
import shutil
import subprocess
from flask import Flask, render_template, request, redirect, url_for, send_from_directory, make_response
from custom import print_Network_info  # getIpAddresses, getHostname, getExtIPAddress
from cartello.cartello import process_markdown as cartello_process_markdown
import datetime
from werkzeug.routing import BaseConverter  # adds support to regex in route
import threading
from http.server import HTTPServer, SimpleHTTPRequestHandler
from argparse import ArgumentParser, RawTextHelpFormatter
import win32api, win32con

from directory_tree_to_html import tree2html_tree_view  # list_files_html
from markdown_render import md_to_html, get_occurrences_from_html_text  # list_files_html
from include_css import add_selector_element

# import Colorama
try:
    from colorama import init, Fore, Back, Style

    if 'PYCHARM_HOSTED' in os.environ:
        convert = False  # in PyCharm, we should disable convert
        strip = False
        print("Hi! You are using PyCharm")
    else:
        convert = None
        strip = None

    init(convert=convert, strip=strip)
except ImportError:
    print('Colorama not imported')

if os.sep == '\\':
    import winreg

if os.path.exists('settings.py'):
    from settings import repos
else:
    repos = []

# import Namespace
try:
    from types import SimpleNamespace as Namespace  # available from Python 3.3
except ImportError:
    class Namespace:
        def __init__(self, **kwargs):
            self.__dict__.update(kwargs)

app = Flask(__name__, static_url_path='/static')
global args
global css_codestyle
global css_docstyle
docstyle_dir = os.path.join(os.getcwd(), 'static', 'css', 'doc_style')
doc_style_files = os.listdir(docstyle_dir)
css_codestyle = None
css_docstyle = None
args = Namespace()
# app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0


# custom routing
class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]


app.url_map.converters['regex'] = RegexConverter
#

# global vars
flaskapp_port = 5001
docs_root = 'docs'
boards_root = os.path.join(docs_root, 'Boards').replace('\\', '/')
docs_root = os.path.realpath(docs_root)
WKHTMLTOPDF_BINARY = ('wkhtmltopdf.exe' if os.sep == '\\' else 'wkhtmltopdf')


def serve_local_docs(port=8000, directory=os.getcwd()):
    class Handler(SimpleHTTPRequestHandler):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, directory=directory, **kwargs)

    httpd = HTTPServer(('localhost', port), Handler)
    httpd.serve_forever()


def get_last_modified_date_time_utc(f, format='%Y-%m-%d %H:%M:%S'):
    modTimesinceEpoc = os.path.getmtime(f)
    modificationTime = datetime.datetime.utcfromtimestamp(modTimesinceEpoc).strftime(format)

    return modificationTime


def get_filetimestamp_now():
    date_time = datetime.datetime.now()  # get datatime info
    dated_text = '%s' % (date_time.day)  # get datatime day in dated
    datem_text = '%s' % (date_time.month)  # get datatime month in datem
    datey_text = '%s' % (date_time.year)  # get datatime year in datey
    date_text = dated_text.rjust(2, '0') + '-' + datem_text.rjust(2, '0') + '-' + datey_text.rjust(4, '0')  # I adjust the date string filling the data and having dd-mm-yyyy
    time_text = date_time.strftime("%X")  # I get the time hh:mm:ss
    file_timestamp = datey_text.rjust(4, '0') + datem_text.rjust(2, '0') + dated_text.rjust(2, '0') + time_text.replace(':', '')

    return file_timestamp


def remove_suffix(text, suffix):
    if text.endswith(suffix):
        return text[:len(text) - len(suffix)]
    return text  # or whatever


def run_process(cmd):
    print('{color}{cmd}{reset_color}'.format(cmd=cmd, color=Fore.CYAN, reset_color=Style.RESET_ALL))
    # process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    # output, err = process.communicate(timeout=3)
    # rc = process.returncode
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    p_out, p_err = process.communicate()
    p_rc = process.returncode

    try:
        color_print = Fore.CYAN + Style.BRIGHT if p_rc == 0 else Fore.RED + Style.BRIGHT
        reset_color = Style.RESET_ALL
    except:
        # colorama not imported
        color_print = ''
        reset_color = ''

    print('{color_print}{text}{reset_color}'.format(text= cmd + '\n' + p_out.decode('utf-8') + '\n' + p_err.decode('utf-8'), color_print=color_print, reset_color=reset_color))

    return p_out, p_err, p_rc


def getRegValue(keyName, valueName):
  reg = winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE)
  key = winreg.OpenKey(reg, keyName)
  value = winreg.QueryValueEx(key, valueName)
  winreg.CloseKey(key)
  return value[0]


def html_to_pdf(input, output, header_url='', footer_url=''):
    generator = ''
    rc = 1  # fake rc since in windows I need to skip chromium
    if os.sep == '/':  # on linux
        generator = 'chromium-browser'
        chromium_cmd = 'chromium-browser --headless --disable-gpu --no-margins --print-to-pdf="{}" {}'.format(output, input)
        output, err, rc = run_process(chromium_cmd)

    if not rc == 0:
        generator = 'google-chrome'
        if os.sep == '/':  # linux
            chrome_exe = 'google-chrome'
        else:  # windows
            chrome_path = getRegValue('SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\chrome.exe', 'Path')
            chrome_exe = os.path.join(chrome_path, 'chrome.exe')

        chrome_cmd = '"{}" --headless --disable-gpu --run-all-compositor-stages-before-draw --no-margins --print-to-pdf="{}" {}'.format(chrome_exe, output, input)
        output, err, rc = run_process(chrome_cmd)

        if not rc == 0:
            generator = 'WKHTMLTOPDF'
            WKHTMLTOPDF_cmd = '"{WKHTMLTOPDF_BINARY}" --header-html {header_url} --footer-html {footer_url} --print-media-type --header-spacing 0  {input_url} {pdf_temp}'.format(
                WKHTMLTOPDF_BINARY=WKHTMLTOPDF_BINARY, header_url=header_url, footer_url=footer_url,
                input_url=input, pdf_temp=output
            )
            # Invoke WkHTMLtoPDF
            result = subprocess.check_output(WKHTMLTOPDF_cmd, shell=True)
            print('"WkHTMLtoPDF" has been used for generating the pdf. However links in the TOC document do not work, please consider installing chromium or chrome.')

    return generator


def get_sidebar_tree(startpath, include_root=True, link_files=None, reg_ext_file_filter=False, reg_ext_dir_filter=False, invert_file_filter=False, invert_dir_filter=False):
    tree_snippet, path_list = tree2html_tree_view.list_files_html(startpath, include_root, link_files, reg_ext_file_filter, reg_ext_dir_filter, invert_file_filter, invert_dir_filter)
    tree_snippet_beginning, tree_snippet_end, html_beginning, html_end = tree2html_tree_view.html_fix_part()
    doc_tree = tree_snippet_beginning + tree_snippet + tree_snippet_end
    doc_tree = os.linesep.join(doc_tree)
    # add margin
    doc_tree = doc_tree.replace('id="myUL"', 'id="myUL" style="margin:10px; padding-bottom: 300px;"')

    return doc_tree, len(max(path_list, key=len))


def download_file(url, filename):
    with open(filename, 'wb') as f:
        response = requests.get(url, stream=True)
        total = response.headers.get('content-length')

        if total is None:
            f.write(response.content)
        else:
            downloaded = 0
            total = int(total)
            for data in response.iter_content(chunk_size=max(int(total/1000), 1024*1024)):
                downloaded += len(data)
                f.write(data)
                done = int(50*downloaded/total)
                sys.stdout.write('\r[{}{}]'.format('█' * done, '.' * (50-done)))
                sys.stdout.flush()
    sys.stdout.write('\n')


def retrieve_git_repo_url(dir):
    origin_cmd = 'cd "{}" && git remote show origin'.format(os.path.join(docs_root, dir))
    cmd_output, cmd_error, cmd_rc = run_process(origin_cmd)

    for l in cmd_output.decode('utf-8').split('\n'):
        if 'Fetch URL:' in l:
            return l.split(':', 1)[1].strip()

    return ''


def retrieve_git_wiki_imgs(doc):
    regex_imgs_link = re.compile(r'!\[.*?\]\((\/uploads\/[A-Za-z0-9/.]+)\)', re.MULTILINE)
    imgs_to_download = re.findall(regex_imgs_link, doc)

    return imgs_to_download


def clone_gitlab_imgs():
    for root, dirs, files in os.walk(docs_root):
        dirs[:] = [d for d in dirs if d.endswith('.wiki')]

        if root == docs_root:
            continue

        repo_link = retrieve_git_repo_url(root)
        uploads_folder = os.path.join(docs_root, root,'uploads')
        if not os.path.exists(uploads_folder):
            os.makedirs(uploads_folder)

        for f in files:
            f_fullfilepath = os.path.join(root, f)
            with open(f_fullfilepath, 'r', encoding='utf-8', errors='ignore') as read_wikidoc:
                wiki_doc = read_wikidoc.read()

            images = retrieve_git_wiki_imgs(wiki_doc)

            for img in images:
                img_full_link = remove_suffix(repo_link, '.wiki.git') + img
                uploads_folder_path_list = [x.replace(':', ':/') for x in os.path.dirname(uploads_folder).split(os.sep) if not x.strip() == '']
                img_path_list = [x for x in img.split('/') if not x.strip() == '']
                img_local_path = os.path.join(*uploads_folder_path_list, *img_path_list)
                img_local_path = '/' + img_local_path if os.sep == '/' else img_local_path  # add slash at beginning, otherwise it's treated as relative path
                if not os.path.exists(img_local_path):
                    os.makedirs(os.path.dirname(img_local_path))
                    download_file(img_full_link, img_local_path)


def update_repos():
    for vcs, directories in repos.items():
        for dir in directories:
            if vcs == 'git':
                pull_cmd = 'cd "{}" && git pull'.format(os.path.join(docs_root, dir))
            else:
                continue
            run_process(pull_cmd)

    # download images for git
    clone_gitlab_imgs()


@app.route('/uploads/<path:filename>')
def send_file(filename):
    """
    function called automatically during the render of a page, in case there images in the gitlab wiki doc.
    Note: gitlab clone doesn't include image download, so that they are downloaded by a snippet in "update_repos()"
    :param filename:
    :return:
    """
    # return send_from_directory('static', '2.png')
    unique_git_directory = os.path.basename(os.path.dirname(filename))
    for root, dirs, files in os.walk(docs_root):
        if os.path.basename(root) == unique_git_directory:
            img_path = os.path.join(root, os.path.basename(filename))
            return send_from_directory(os.path.dirname(img_path), os.path.basename(filename))

    return send_from_directory('static', 'not_found.png')


@app.route('/search', methods=['GET', 'POST'])
def search():
    update_repos()
    if request.method == 'POST':
        criteria = request.form.get("criteria")
        regex_flag = request.form.get("regex_flag")
        ignorecase_flag = request.form.get("ignorecase_flag")

        search_results_dict = {}
        for r, d, files in os.walk(docs_root):
            for f in files:
                if not f.lower().endswith('.md'):
                    continue
                fullfilepath = os.path.join(r, f)
                relpathfile = os.path.relpath(fullfilepath, docs_root)
                doc_html = md_to_html(fullfilepath)
                search_results_dict[relpathfile] = get_occurrences_from_html_text(doc_html, criteria, regex_flag, ignorecase_flag)

        search_results_dict_filtered = {k: v for (k, v) in search_results_dict.items() if v > 0}  # only contains files with occurrences found

        if len(search_results_dict_filtered) > 0:
            no_text = 'R'
        else:
            no_text = 'No r'

        doc_tree, len_sidebar = get_sidebar_tree(docs_root, False, '/view/', r'\.md$', r'uploads', False, True)

        options = []
        if regex_flag:
            options.append("regex")
        if ignorecase_flag:
            options.append("ignorecase")
        options = ' - '.join(options)

        return render_template('search.html', doc_tree=doc_tree, search_results=search_results_dict, no_text=no_text, search_criteria=criteria.replace(' ', '&nbsp;'), options=options, len_sidebar=len_sidebar, pdf_link='', docs_root=os.path.basename(docs_root))

    else:  # 'GET':
        return home()


@app.route("/pdf/<path:page>")
def wiki_pdf(page):
    update_repos()
    file_path = os.path.abspath(os.path.join(os.path.dirname(docs_root), page))
    if not os.path.isfile(file_path):
        return "file doesn't exist."

    if '.md' not in [ext.lower() for ext in os.path.splitext(file_path)]:
        return send_from_directory(os.path.dirname(file_path), os.path.basename(file_path))

    # Configure the different paths.
    pdf_temp = get_filetimestamp_now() + '.pdf'
    input_url = url_for('route_doc_links', file=page, _external=True)
    header_url = '' # url_for('print_header', _external=True)
    footer_url = ' '# url_for('print_footer', _external=True)

    # create pdf
    generator = html_to_pdf(input_url, os.path.join(os.path.dirname(docs_root), pdf_temp))

    # Write the newly generated temp pdf into a response.
    with open(pdf_temp, 'rb') as f:
        binary_pdf = f.read()
        # binary_pdf = re.sub(b'(\/URI)\s+(.*)(#toc-\d+)', b'/Dest /toc-1>>', binary_pdf)  # fix toc link attempt
        target_file_name = page.replace("/", "_").replace("\\", "_")
        response = make_response(binary_pdf)
        response.headers['Content-Type'] = 'application/pdf'
        response.headers['Content-Disposition'] = 'inline; filename={}.pdf'.format(target_file_name)

    # Delete the temp file and return the response.
    try:
        os.remove(pdf_temp)
    except:
        pass

    return response


@app.route('/')
def home():
    update_repos()
    doc_tree, len_sidebar = get_sidebar_tree(docs_root, False, '/view/', r'\.md$', r'uploads', False, True)

    return render_template('index.html', css_docstyle=css_docstyle, doc_style_files=doc_style_files, css_codestyle=css_codestyle, doc_tree=doc_tree, len_sidebar=len_sidebar, pdf_link='docs/home.md')


@app.route('/view/<regex(".+"):file>')
def route_doc_links(file):
    """
    :param file: possible inputs: 1) .md for docnado ; 2) .md for board (/docs/Boards)
    :return:
    """
    update_repos()
    doc_tree, len_sidebar = get_sidebar_tree(docs_root, False, '/view/', r'\.md$', r'uploads', False, True)

    fullpathfile = os.path.realpath(file)
    if not os.path.exists(fullpathfile):
        return 'The file does not exists'  # TODO: return the home page for board

    if not fullpathfile.lower().endswith('.md'):
        return 'The file is not markdown'  # TODO: return the home page for board

    # get the document type (docnado or cartello (board) )
    board_file = False
    doc_file = False
    filepath_splitted = file.split('/')
    if len(filepath_splitted) > 1:
        if filepath_splitted[1] == 'Boards':
            board_file = True
        else:
            doc_file = True
    else:
        doc_file = True

    # render the correct page
    if board_file:
        f_board_title = os.path.splitext(os.path.basename(fullpathfile))[0]
        doc_markdown = cartello_process_markdown(fullpathfile, f_board_title.title())

    elif doc_file:
        doc_markdown = md_to_html(fullpathfile, False, wrap_in_html=False)

    pdf_link = url_for('wiki_pdf', page=file, _external=True)
    return render_template('index.html', css_docstyle=css_docstyle, doc_style_files=doc_style_files, css_codestyle=css_codestyle, doc_tree=doc_tree, doc_markdown=doc_markdown, board=board_file, len_sidebar=len_sidebar, pdf_link=pdf_link)


def check_args():
    parser = ArgumentParser(formatter_class=RawTextHelpFormatter, description="""
    Description

    """)

    # Options
    parser.add_argument("-H", "--header", dest="header", default=None,
                        help="specify a custom html file for using it as header")

    parser.add_argument("-F", "--footer", dest="footer", default=None,
                        help="specify a custom html file for using it as footer")

    parser.add_argument("-dc", "--custom-doc-css", dest="custom_doc_css", default=None, nargs="*",
                        help="specify a custom css file for using a custom style")

    parser.add_argument("-cc", "--custom-code-css", dest="custom_code_css", default=None, nargs="*",
                        help="specify a custom css file for using a custom style")
                        # action='store_true'
                        # nargs="+",
                        # nargs='?',  # optional argument
                        # default=""
                        # type=int
                        # choices=[]

    args = parser.parse_args()  # it returns input as variables (args.dest)

    # end check args

    return args


# custom Filters
@app.template_filter()
def pdffilename(doc_link):
    return remove_suffix(os.path.basename(doc_link), '.md') + '.pdf'

@app.template_filter()
def get_css_title(css_file):
    return remove_suffix(css_file.lower().replace(' ', '_'), '.css').title()

#


def main():
    global args
    global css_codestyle
    global css_docstyle
    args = check_args()

    # I obtain the app directory
    if getattr(sys, 'frozen', False):
        # frozen
        dirapp = os.path.dirname(sys.executable)
        dirapp_bundle = sys._MEIPASS
        executable_name = os.path.basename(sys.executable)
    else:
        # unfrozen
        dirapp = os.path.dirname(os.path.realpath(__file__))
        dirapp_bundle = dirapp
        executable_name = os.path.basename(__file__)

    if args is None:
        args = check_args()

    # copy the custom static files as set in input
    static_dir = os.path.join(dirapp, 'static')
    static_css_dir = os.path.join(static_dir, 'css')
    static_docstyle_css_dir = os.path.join(static_dir, 'css', 'doc_style')
    static_sourcecode_css_dir = os.path.join(static_dir, 'css', 'src')

    # doc_style
    if args.custom_doc_css:
        for custom_doc_css in args.custom_doc_css:
            if os.path.exists(custom_doc_css):
                with open(custom_doc_css, 'rb') as read_css:
                    css_content = read_css.read()
                dest_css = os.path.join(static_docstyle_css_dir, os.path.basename(custom_doc_css))
                css_docstyle = os.path.basename(custom_doc_css).replace(' ', '_')

                new_css_content = add_selector_element(css_content, '.doc ')

                with open(dest_css, 'w') as write_new_css:
                    write_new_css.writelines(new_css_content)

            else:
                pass
                #print('{} does not exist. Loading default "custom_doc_css"'.format(args.custom_doc_css))

    # code_style
    if args.custom_code_css:
        for custom_code_css in args.custom_code_css:
            if os.path.exists(custom_code_css):
                with open(custom_code_css, 'rb') as read_css:
                    css_content = read_css.read()
                dest_css = os.path.join(static_sourcecode_css_dir, os.path.basename(custom_code_css))
                css_codestyle = os.path.basename(custom_code_css).replace(' ', '_')

                new_css_content = add_selector_element(css_content, '.doc ')

                with open(dest_css, 'w') as write_new_css:
                    write_new_css.writelines(new_css_content)
            else:
                pass
                #print('{} does not exist. Loading default "custom_code_css"'.format(args.custom_code_css))

    app.run(host="0.0.0.0", port=flaskapp_port, debug=True)


if __name__ == '__main__':
    main()


"""
changes made in "cartello" module

* strippedNames argument in "process_markdown" function, must have a default value () process_markdown(fp, current, strippedNames=())
* removed tag <div class="hero-foot">

"""

# * https://stackoverflow.com/questions/34901523/file-url-not-allowed-to-load-local-resource-in-the-internet-browser/42294756

# TODO:
# DONE: code with no language in fences, is now red and treated as any "code" string
# DONE: filter is stuck on the left
# DONE: remove circle in navbar

# TODO:
# DONE: switch documents/search
# DONE: doc link must be chapter name
# DONE: search options
# TODO: multiple repos?
# TODO: header / footer
# TODO: export single folder in pdf
# TODO: export entire notebook in pdf
# TODO: export cmd usage

# DONE: exporting as pdf does not make the TOC working (they work with Print->Pdf)
# DONE: the link for pdf must be the same as view, just change to pdf
# DONE: convert html to pdf with chrome/chromium when available
# DONE: TOC style: page break after toc
# TODO: set margins for print pdf

# First release:
# TODO: design input repos (git/dropbox/paper/googledrive)
# DONE: resize sidebar according to the longest file (use monospace?)
# DONE: add support for icons
# DONE: outline (toc) in sidebar with options: display in document, include in print/export
# TODO: scrollbar in sidebar
# DONE: click on the paragraph, it moves there but the header is covered by the navbar
# TODO: [BUG] - http://127.0.0.1:5001/view/docs/Boards/test.md does not set sidebar_len variable
# TODO: dropdown menu in navbar for switching the theme at runtime (Perforce, HelixQAC, Mozilla, Confluence)
# DONE: TOC must be located where [TOC] text is present in the document. currently is always on top
# DONE: [BUG] - at beginning the outline is not hidden in the sidebar-> files
# DONE: [BUG] - Search: add padding on top
# DONE: [BUG] - Search: remove scroll page
# DONE: [BUG] - if empty, don't make the search (stop the submit)

# TODO: [BUG] [RENDER] -    > Note1: Virtual-Box and Vmware are not going to work anymore since they require Hyper-V disabled
#    >
#    > Note2: To disable it again use
#    >
#    > ```
#    > dism.exe /Online /Enable-Feature:Microsoft-Hyper-V /All
#    > ```
#    >
#    > or
#    >
#    > ```
#    > bcdedit /set hypervisorlaunchtype off
#    > ```
#    >
#    > ​
#
# is displayed with different boxes instead of a single one