import re, io, os, sys, shutil
sys.path.append("..")
sys.path.append("modules")
from bs4 import BeautifulSoup
import markdown2 as markdown
import mistune
from mistune_contrib.toc import TocMixin
from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import html
from argparse import ArgumentParser, RawTextHelpFormatter


def check_args():
    parser = ArgumentParser(formatter_class=RawTextHelpFormatter, description="""
    Description

    """)

    # Options
    parser.add_argument("-dc", "--custom-doc-css", dest="custom_doc_css",
                        help="specify a custom css file for using a custom style")

    parser.add_argument("-cc", "--custom-code-css", dest="custom_code_css",
                        help="specify a custom css file for using a custom style")
                        # action='store_true'
                        # nargs="+",
                        # nargs='?',  # optional argument
                        # default=""
                        # type=int
                        # choices=[]

    args = parser.parse_args()  # it returns input as variables (args.dest)

    # end check args

    return args
"""
def convert_md_html_mistune(md):
    class EmojiRenderer(object):
        def emoji(self, text):
            return "<emoji>%s</emoji>" % text

    class EmojiInlineLexer(mistune.InlineLexer):
        def __init__(self, **kwargs):
            super(EmojiInlineLexer, self).__init__(**kwargs)
            self.default_rules.insert(0, "emoji")
            self.rules.emoji = re.compile(r'^:([a-zA-Z0-9\+\-_]+):', re.I)

        def output_emoji(self, m):
            text = self.output(m.group(1))
            return self.renderer.emoji(text)

    class MarkdownRenderer(mistune.Renderer, EmojiRenderer):
        def __init__(self, **kwargs):
            super(MarkdownRenderer, self).__init__(**kwargs)

        def block_code(self, code, lang):
            if not lang:
                return '\n<pre><code>%s</code></pre>\n' % \
                       mistune.escape(code)
            lexer = get_lexer_by_name(lang, stripall=True)
            formatter = html.HtmlFormatter()
            return highlight(code, lexer, formatter)

    # renderer = HighlightRenderer()
    renderer = MarkdownRenderer()
    inline = EmojiInlineLexer(renderer=renderer)
    markdown = mistune.Markdown(renderer=renderer, inline=inline)

    return markdown(md)
"""


def __convert_md_html_mistune(md, include_toc=False):
    class EmojiRenderer(object):
        def emoji(self, text):
            return "<emoji>%s</emoji>" % text

    class EmojiInlineLexer(mistune.InlineLexer):
        def __init__(self, **kwargs):
            super(EmojiInlineLexer, self).__init__(**kwargs)
            self.default_rules.insert(0, "emoji")
            # self.rules.emoji = re.compile(r'^:([a-zA-Z0-9\+\-_]+):', re.I)
            self.rules.emoji = re.compile(r':([a-zA-Z0-9\+\-_]+):', re.I)

        def output_emoji(self, m):
            text = self.output(m.group(1))
            return self.renderer.emoji(text)

    class MarkdownRenderer(mistune.Renderer, EmojiRenderer):
        def __init__(self, **kwargs):
            super(MarkdownRenderer, self).__init__(**kwargs)

        def block_code(self, code, lang):
            if not lang:
                return '\n<pre><code>%s</code></pre>\n' % \
                       mistune.escape(code)
            lexer = get_lexer_by_name(lang, stripall=True)
            formatter = html.HtmlFormatter()
            return highlight(code, lexer, formatter)

    # renderer = HighlightRenderer()
    renderer = MarkdownRenderer()
    inline = EmojiInlineLexer(renderer=renderer)
    markdown = mistune.Markdown(renderer=renderer, inline=inline)
    doc_html = markdown(md)

    if include_toc:
        class TocRenderer(TocMixin, mistune.Renderer):
            pass

        toc = TocRenderer()
        md_toc = mistune.Markdown(renderer=toc, inline=inline)
        toc.reset_toc()
        md_toc.parse(md)
        toc_html = toc.render_toc(level=3)
        # rv = rv.replace('\n', '').replace(' ', '')

        toc_text = '<p>[TOC]</p>'
        if toc_text in doc_html:
            doc_html = doc_html.replace(toc_text, toc_html)
        else:
            doc_html = toc_html + doc_html

    return doc_html


def convert_md_html_mistune(md, include_toc=False):
    class HighlightRenderer(mistune.Renderer):
        def block_code(self, code, lang):
            if not lang:
                return '\n<pre><code>%s</code></pre>\n' % \
                       mistune.escape(code)
            lexer = get_lexer_by_name(lang, stripall=True)
            formatter = html.HtmlFormatter()
            return highlight(code, lexer, formatter)

    renderer = HighlightRenderer()
    markdown = mistune.Markdown(renderer=renderer)
    doc_html = markdown(md)

    if include_toc:
        class TocRenderer(TocMixin, mistune.Renderer):
            pass

        toc = TocRenderer()
        md_toc = mistune.Markdown(renderer=toc)
        toc.reset_toc()
        md_toc.parse(md)
        toc_html = toc.render_toc(level=3)
        # rv = rv.replace('\n', '').replace(' ', '')

        toc_text = '<p>[TOC]</p>'
        if toc_text in doc_html:
            doc_html = doc_html.replace(toc_text, toc_html)
        else:
            doc_html = toc_html + doc_html

    return doc_html


def get_occurrences_from_html_text(html_file, text, regexFlag, ignorecaseFlag):
    #soup = BeautifulSoup(html_file, features="lxml")
    soup = BeautifulSoup(html_file, "html.parser")

    page_md = soup.findAll()

    tag_text = []
    [tag_text.append(x.text) for x in page_md]
    tag_text = '\n'.join(tag_text)

    if regexFlag:
        regex_flags = []
        if ignorecaseFlag:
            regex_search = re.compile(text, re.IGNORECASE)
        else:
            regex_search = re.compile(text)
        occurrences = len(re.findall(regex_search, tag_text))
    else:
        if ignorecaseFlag:
            occurrences = tag_text.lower().count(text.lower())
        else:
            occurrences = tag_text.count(text)

    return occurrences


def convert_md_html(md_text):
    html = markdown.markdown(md_text, extras=["footnotes", "fenced-code-blocks"])

    return html


def wrap_html(body,title='', css=[], js=[]):
    css_text = []
    js_text = []

    for css_file in css:
        css_text.append('<link rel="stylesheet" href="{}">'.format(css_file))

    for js_file in js:
        js_text.append('<script src="{}"></script>'.format(js_file))

    html = """<!DOCTYPE html>
<html>
<head>
<title>{title}</title>

{css_text}

</head>
<body>

{body}

{js_files}
<script>
{js_text}
</script>

</body>
</html>""".format(body=body, title=title, css_text='\n'.join(css_text), js_text='', js_files='\n'.join(js_text))

    return html


def md_to_html(md_file, html_file=None, wrap_in_html=False, css_doc='doc_style.css', css_syntax_highlighting='pygments.css', js=[]):
    md_html = convert_md_html_mistune(io.open(md_file, "r", encoding='utf-8', errors='ignore').read(), include_toc=True)

    if wrap_in_html:
        doc_html = wrap_html(md_html, '', ['css/' + css_syntax_highlighting, 'css/' + css_doc], js)
    else:
        doc_html = md_html

    if html_file:
        with open(html_file, 'w') as write_html:
            write_html.write(doc_html)

    return doc_html


def get_occurrences(word, md_file, regexFlag, ignorecaseFlag):
    # convert markdown to html
    doc_html = md_to_html(md_file, 'html.html', wrap_in_html=True, css_doc='doc_style.css', css_syntax_highlighting='pygments.css')

    return get_occurrences_from_html_text(doc_html, word, regexFlag, ignorecaseFlag)


def main(args=None):
    if args is None:
        args = check_args()

    # TEST
    filelist = ['tooltip.md']
    occurrences_dict = {}  # {file : occurrences}
    for f in filelist:
        # convert markdown to html
        css_location = 'css'  # copy the custom css to the css location
        if args.custom_code_css:
            if os.path.exists(args.custom_code_css):
                css_doc = os.path.basename(args.custom_code_css)
                shutil.copy2(args.custom_code_css, css_location)
            else:
                css_doc = 'doc_style.css'
        else:
            css_doc = 'doc_style.css'

        if args.custom_doc_css:
            if os.path.exists(args.custom_doc_css):
                css_syntax_highlighting = os.path.basename(args.custom_doc_css)
                shutil.copy2(args.custom_doc_css, css_location)
            else:
                css_syntax_highlighting = 'pygments.css'
        else:
            css_syntax_highlighting = 'pygments.css'

        js = ['js/callouts.js']

        doc_html = md_to_html(f, 'html.html', wrap_in_html=True, css_doc=css_doc, css_syntax_highlighting=css_syntax_highlighting, js=js)

        occ = get_occurrences_from_html_text(doc_html, 'button', True, True)
        occurrences_dict[f] = occ

    print(occurrences_dict)


if __name__ == '__main__':
    main()

"""
p_elements = document.getElementsByTagName('p');

for(i=0; i < p_elements.length; i++){
    tags = ['{: .info}', '{: .warning}', '{: .danger}', '{: .tip}']
    
    for (j=0; j < tags.length; j++){
        let tag = tags[j];
        if (p_elements[i].innerText.endsWith(tag)){
            class_name = tag.replace('{: .', '').replace('}', '');
            p_elements[i].classList.add(class_name);
            regex_replace = new RegExp(tag + "$", "gi");
            p_elements[i].innerText = p_elements[i].innerText.replace(regex_replace, '')
        }
    }
}
"""