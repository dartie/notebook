/* Callouts */
p_elements = document.getElementsByTagName('p');

for(i=0; i < p_elements.length; i++){
    tags = ['{: .info}', '{: .warning}', '{: .danger}', '{: .tip}']

    let p_processed = false;
    for (j=0; j < tags.length; j++){
        let tag = tags[j];
        if (p_elements[i].innerText.endsWith(tag)){
            class_name = tag.replace('{: .', '').replace('}', '');
            p_elements[i].classList.add(class_name);
            regex_replace = new RegExp(tag + "$", "gi");
            p_elements[i].innerText = p_elements[i].innerText.replace(regex_replace, '');
            p_processed = true;
        }
    }

    /* code: remove text NOTE: at the beginning */
    // removed since there is a confict with emoji
//    if (! p_processed){
//        regex_replace_note = new RegExp("^note\s*:?", "gi");
//        p_elements[i].innerText = p_elements[i].innerText.replace(regex_replace_note, "")
//    }
}
