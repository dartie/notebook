# Notebook
Notebook is a webserver for rendering markdown documents in order to display them when needed.

The aim is have all the documentation in one place, allowing a quick export in case of documentation delivery.

The style must be flexible, so that it can customized according to the organization.
Style is handled with css, which can be changed or injected, according to the needs.

## List of implemented features
- [X] File Filter
- [X] Search in content (regex support)
- [X] Render of markdown files
    - [X] TOC (Table of Content)
    - [X] callouts 
    - [X] emoji
    - [X] TODO checkbox
    - [X] Source code syntax highlighting
    - [X] Kanban boards (see documentation below for syntax)
    
- [X] Export to PDF
- [X] Print
- [X] Support to GIT: all repository located under the documentation root (`docs`) are updated automatically 


## TODO
- [ ] https://pypi.org/project/pypandoc/
- [ ] Render of markdown files
    - [ ] latex
    - [ ] graphs
    - [ ] slides
    - [ ] number of lines for source code fences


## Installation

### Requirements
The app is built on Flask 
* `python` (tested on python 3, but written back compatible with python2)
* python packages needed are listed in the `requirements.txt`
* `Chrome`: it's not mandatory but recommended as it's used for the pdf export. If missing the built-in pdf converter is used, but it's slower and the toc link support is missing

### Steps
1. Download and install python
2. Install python packages required using `pip install -r requirements.txt` from the root directory
3. Run the server using `python app.py`
4. Place all documentation to render in the `docs` directory 
4. Connect from a client