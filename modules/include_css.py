import tinycss
import re


def add_selector_element(css_content, selector_element):
    parser = tinycss.make_parser('page3')
    stylesheet = parser.parse_stylesheet_bytes(css_content)

    locations = []
    locations_dict = dict()
    for rule_keyword in stylesheet.rules:
        if isinstance(rule_keyword, tinycss.css21.RuleSet):
            regex_location = re.compile(r'(\d+):(\d+)')  # row:column
            match = re.findall(regex_location, str(rule_keyword))
            if match is not None:
                match = match[0]
                if len(match) == 2:
                    row = int(match[0])
                    col = int(match[1])
                    locations.append([row, col])

                    if row not in locations_dict:
                        locations_dict[row] = []
                    locations_dict[row].append(col)

    css_content_decoded = css_content.decode('utf-8')
    # css_content_decoded = re.split(r'([\n])', css_content_decoded)
    # css_content_decoded = [x for x in css_content_decoded if not x == '']
    css_content_decoded = css_content_decoded.split('\n')

    new_css_content = []

    line_index = 0
    for line in css_content_decoded:
        line_index += 1

        if line_index in locations_dict:
            for col in locations_dict[line_index]:
                new_text = line[:col-1] + selector_element + line[col-1:]
                new_css_content.append(new_text)
        else:
            new_css_content.append(line)

    return new_css_content
