Title:      Test
desc:       Test
template:   document
nav:        Projects>Test
date:       2019/05/16

Dev Projects
===

### Markdown Notebook

* `docnado` docnado for rendering Markdown files: https://heinventions.github.io/docnado-site/ - https://github.com/HEInventions/docnado
* add meta-data
```
: :
Title:      Title
desc:       Desc
template:   document
nav:        docs>Python
date:       2018/07/20
```
* `cartello` cartello for rendering Markdown Kanban tools: https://github.com/FraMecca/Cartello
* `cartello` fix broken links
* `danger|TO DO` create a script which prepare files to be served (clone gitlab, Markdown Notebook Dropbox)


### TODO

* Public progress bar Project
* Public compare project
* Public class for running process


