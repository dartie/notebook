function ToggleNav(mySidebar_id="mySidebar", main_id="main") {
  let sidebar = document.getElementById("mySidebar");
  let main = document.getElementById("main");

  if (sidebar.style.width == "0px" || sidebar.style.width == "0" || sidebar.style.width == ""){
	sidebar.style.width = len_sidebar;
  	main.style.marginLeft = len_sidebar;

  	/* set width "view_buttons" */
  	view_buttons.style.width = len_sidebar;
  	view_buttons.style.display = "block";
  }
  else{
	sidebar.style.width = "0px";
  	main.style.marginLeft = "0px";
  	view_buttons.style.width = "0px";
    view_buttons.style.display = "none";
}

}


function closeNav(mySidebar_id="mySidebar", main_id="main") {
    document.getElementById(mySidebar_id).style.width = "0";
    document.getElementById(main_id).style.marginLeft= "0";
}

function openNav(mySidebar_id="mySidebar", main_id="main") {
    /* pre-set */
    document.getElementById(mySidebar_id).style['-webkit-transition-duration'] = "0s";
    document.getElementById(main_id).style['-webkit-transition-duration'] = "0s";

    /* open nav */
    document.getElementById(mySidebar_id).style.width = "250px";
    document.getElementById(main_id).style.marginLeft = "250px";

    /* restore settings */
    document.getElementById(mySidebar_id).style['-webkit-transition-duration'] = "0.25s";
    document.getElementById(main_id).style['-webkit-transition-duration'] = "0.25s";

    /* display buttons switch view */
    view_buttons.style.width = len_sidebar;

}

function select_view(view) {
    /* select "Files" or "Outline" in the sidebar */
    var files_btn = document.getElementsByClassName('sidebar_split_left')[0];
    var outline_btn = document.getElementsByClassName('sidebar_split_right')[0];

    var files_view_hidden = document.getElementsByClassName('myUL');
    var files_view = document.getElementById('myUL');
    var outline_view = document.getElementById('myoutline');

    if (view == 'files') {
        for (i=0; i < files_view_hidden.length; i++){
            files_view_hidden[i].style.display = "block";
        }
        files_view.style.display = "block";

        outline_view.style.display = "none";
        files_btn.classList.add("sidebar_active");
        outline_btn.classList.remove("sidebar_active");
    } else {
        for (i=0; i < files_view_hidden.length; i++){
            files_view_hidden[i].style.display = "none";
        }
        files_view.style.display = "none";

        outline_view.style.display = "block";
        outline_btn.classList.add("sidebar_active");
        files_btn.classList.remove("sidebar_active");
    }
}

function display_toc(){
    var toc = document.getElementById('table-of-content');
    var toc_sidebar_div = document.getElementById("myoutline");

    if (! toc){
        return;
    }

    var toc_clone = toc.cloneNode(true);
    toc_clone.id = "table-of-content-sidebar";

    toc_sidebar_div.appendChild(toc_clone);
}

display_toc();
//select_view('files');


/* resize height of sidbar according to the navbar */
window.addEventListener("resize", resize_sidebar);

function resize_sidebar() {
  let topnav_height = document.getElementById('topnav').clientHeight;
  document.getElementById('mySidebar').style.top = topnav_height;
}