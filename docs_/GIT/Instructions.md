Title:      Instructions
desc:       Instructions
template:   document
nav:        Git>Instructions
date:       2018/01/19

# GIT

## Original Help 
```
Command line instructions


Git global setup

git config --global user.name "Dario Necco"
git config --global user.email "dario_necco@programmingresearch.com"

Create a new repository

git clone http://mist.programmingresearch.com/csgtools/SMGenerator.git
cd SMGenerator
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin http://mist.programmingresearch.com/csgtools/SMGenerator.git
git add .
git commit
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin http://mist.programmingresearch.com/csgtools/SMGenerator.git
git push -u origin --all
git push -u origin --tags
```

## Add a file and push automatically to GIT
git add "file" && git commit -m " " && git push -u origin master


## Store credentials permanently 
```
git config credential.helper store
git push http://example.com/repo.git
```